# KiCad Demo Server

# First Time Running

0. Make sure you have `node 12` and `npm` on your machine
1. Install decencies by running

```
$ npm i
```

2. Run server by using

```
$ node mock-kicad-server.js
```

3. Follow CLI prompts

# Command Line Arguments

To see the command line arguments run the following

```
$ node mock-kicad-server.js --help
```

# Reading the API docs
See https://docs.google.com/document/d/1ibc1CtqaPsl_z6HTpUybgDpXglcrLUYAqHou1LAIXf4/edit?usp=sharing
