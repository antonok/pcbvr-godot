
### Version

Repo: https://github.com/fire/godot.git
commit: f00f49cf05c39db0d985e4327594bb10d5547ca4

### How to do it

Once you have you're 3D scene setup, you can add a spatial node that will use to add the circuit board gltf under. 

The circuit board needs to be in in `user://` directory

You can find out what the system uses for it's user directory with this print command
```swift
print(OS.get_user_data_dir())
```

