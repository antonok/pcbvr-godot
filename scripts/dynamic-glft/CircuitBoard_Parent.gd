extends Spatial

# Called when the node enters the scene tree for the first time.
func _ready():
	
	var importer = SceneImporterGLTF.new()
	var b_cu = Spatial.new()
	
	b_cu.add_child(importer.import_gltf("user://B.Cu.glb"))
	
	b_cu.translate(Vector3(-30, 25, 0.177))
	
	add_child(b_cu)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_y(deg2rad(1))
