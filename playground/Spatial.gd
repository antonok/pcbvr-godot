
extends Node


var ws = null

func _ready():
	ws = WebSocketClient.new()
	ws.connect("connection_established", self, "_connection_established")
	ws.connect("connection_closed", self, "_connection_closed")
	ws.connect("connection_error", self, "_connection_error")
	ws.connect("data_received", self, "_data_recieved")
	
	var url = "ws://localhost:8080"
	print("Connecting to " + url)
	ws.connect_to_url(url)

func _connection_established(protocol):
	print("Connection established with protocol: ", protocol)
	ws.get_peer(1).put_var("hello-from-godot")

func _data_recieved():
	if ws.get_peer(1).is_connected_to_host():
		if ws.get_peer(1).get_available_packet_count() > 0 :
			var test = ws.get_peer(1).get_packet().get_string_from_utf8() 
			print('recieve %s' % test)
	
	
func _connection_closed():
	print("Connection closed")

func _connection_error():
	print("Connection error")

func _process(delta):
	if ws.get_connection_status() == ws.CONNECTION_CONNECTING || ws.get_connection_status() == ws.CONNECTION_CONNECTED:
		ws.poll()
