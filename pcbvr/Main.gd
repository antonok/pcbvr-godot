extends Spatial

"""
this node is the main node of the project it is anglonous to the main
function in C. This node is mostly just scaffolding for the child nodes such as
dynamic board loading.
"""


var vr_cam;

var VR_VIEW = true;

# Called when the node enters the scene tree for the first time.
func _ready():
	print('see documentation in README.md');
	# connects the network emition to the callback on the board
	$Networking.connect("load_board", $Board, "load_layer_gltf");
	## $Networking.connect("clear_board", $Board, **TODO**);
	## $Networking.connect("gobal_position_offset", $Board, "set_global_offset");
	$Networking.connect("end_vr", self, "_quit");
	$"Viewport-VR/ARVROrigin/Left_hand".connect("_set_camera_transform",self, "move_camera_to_controller");
	$"Viewport-VR/ARVROrigin/Left_hand/Function_pointer2".connect("pointer_pressed", $"Viewport-VR/ARVROrigin/Left_hand/pcbvr_menus/menu1", "_on_pointer_pressed");
	# starts the networking
	$Networking.start(GLOBAL.KICAD_SERVER_PORT);
	var interface = ARVRServer.find_interface("OpenVR");
	if interface and interface.initialize():
		
		#Make sure GoDot knows about the size of our viewport, otherwise it is only known inside of our render driver
		$"Viewport-VR".size = interface.get_render_targetsize();
		
		#Tell our viewport it is the ARVR viewport
		$"Viewport-VR".arvr = true;
		
		# Turn off vsync off to increase Frame rate
		OS.vsync_enabled = false;
		
		# Change our physics engine frame rate
		Engine.target_fps = 90
		
		# Tell our display what we want to display
		$"ViewportContainer/Viewport-UI".set_viewport_texture($"Viewport-VR".get_texture());
		
	vr_cam = self.get_node("Viewport-VR/ARVROrigin/ARVRCamera");
	if VR_VIEW:
		vr_cam.get_node("oculus").visible = false;
func _process(delta):
	if VR_VIEW:
		$Camera.transform = vr_cam.transform;

func _input(e):
	if Input.is_key_pressed(KEY_F1):
		VR_VIEW = !VR_VIEW;
	vr_cam.get_node("oculus").visible = not VR_VIEW;
#	$Sky_texture.set_time_of_day(10, get_node("DirectionalLight"), deg2rad(0));
	
#func _on_Sky_texture_sky_updated():
#	$Sky_texture.copy_to_environment(get_viewport().get_camera().environment)
func move_camera_to_controller(t):
	$Camera.transform = t;

func _quit(message: String, in_error: bool):
	# TODO: show a pop up to the user if quiting due to an error?
	print("Quiting: ", message);
	assert(!in_error);
	get_tree().quit();
