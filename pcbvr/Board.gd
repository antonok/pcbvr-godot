extends Spatial

"""
This is the main controller for all the layers of the board and interaction.

"""

const ROTATION_SPEED := .01
const KEYBOARD_MOVE_SPEED := .1
const MIN_SEPERATION := .1
const EXPANSION_RATE := .1

"""
_render_data contains all the information for the baord to be drawn.
It contains sptail objects for the layers and vias to be rendered and get
drawn on each _update_children call.

Design considerations:

	As of right now we have a fixed 'z_offset'. howeveer this may change,
	let's say the user wants to pull one layer from another and just view one
	layer.

_render_data = {
	<layerNumber>: {
		'z_offset': offset for when expanding,
	   'spatial': Spatial_GLTF_Object,
	   'throughHoleMaskPath':  ''</full/path/to/*.png>'' | null,
	   'below_via_spatial': Spatial_GLTF_Object
	   'displayName': ''<some_name_that_makes_sense>'' | null
	}
	…(rest of board layers)
}
"""
var _render_data: Dictionary;
var _offset_in_z_per_layer: float = .5;

# is a movement_state
var _is_expanding: int;
# Object that is used to import spatial object
var _importer = SceneImporterGLTF.new();

#TODO undo hardcoding
var _global_offset: Dictionary = {'x': -14.501148700714111, 'y': 6.862429738044739};

# Determines state of StaticBoard
enum movement_state {
	stationary = 0,
	expanding = 1,
	shrinking = -1
}

const NET_HIGHLIGHT := false;

# Called when the node enters the scene tree for the first time.
func _ready():
	_is_expanding = movement_state.stationary;
	print('File path to put GLTF at, the user:// directory (.glb file should be in directory to read from)')
	print(OS.get_user_data_dir());

# Queues the deletion of each child node and
# removes it from board
func _reset_children():
	for child in get_children():
		child.queue_free();
		self.remove_child(child);

"""
How our Spatial Board node is laid out
└── Board
	├── Layer_0
	|   └── via_0-1
	├── Layer_1
	|   └── via_1-2
	├── Layer_2
	|   └── via_2-3
	└── Layer_3

## Design choice considerations

This will allow us to keep the childern of board at a consistent seperation
in the z direction when expanding. Also the vias are attached to the layers
so that offsets is relative to the layer and not to the board

Please note that the last layer will not have another child node
"""

var sentinel_midpoint;
var grids = [null, null, null];

# Adds a bunch of child nodes that are represented by _render_data
func _update_children():
	# Invariant
	assert(len(_render_data) == 1 or len(_render_data) % 2 == 0);
	assert(len(_render_data) >= self.get_child_count());

	# There are too many edge cases when adding or subtracting layers into an
	# exisiting board
	# TODO - Add multiple layer adding without reseting each time for
	# performance optimatizations
	self._reset_children();

	for layer_num in range(len(_render_data)):
#		print_debug("getchildcount" + str(self.get_child_count()));
		if layer_num >= self.get_child_count() - 1:
			self.add_child(_render_data[layer_num]['spatial']);
			self.get_child(self.get_child_count() - 1).add_child(
				_render_data[layer_num]['below_via_spatial']);

		elif not self.get_child(layer_num) is _render_data[layer_num]['spatial']:
			var child_to_replace := self.get_child(layer_num);
			self.remove_child(child_to_replace);
			child_to_replace.queue_free();
			self.add_child(_render_data[layer_num]['spatial']);
			self.move_child(self.get_child(self.get_child_count() - 1), layer_num);
		else:
			print_debug("Should never arrive here");
			assert(0);

	_move_all_children(_global_offset.x, _global_offset.y);
	
	#SUPER HACKY CODE HERE FOR THE GRID PLEASE IGNORE
	var layer_grid_idx = self.get_child_count() - 1;
	
	var ref_grid = self.get_parent().get_node("Grid");
	
	var grid = ref_grid.duplicate()
	assert(grid != null);
	self.grids[0] = grid
	self.grids[1] = grid.duplicate()
	self.grids[1].rotation_degrees = (Vector3(0, 180, 0));
	self.get_child(layer_grid_idx).add_child(grids[0]);
	self.get_child(layer_grid_idx).add_child(grids[1]);
	self.grids[2] = self.get_parent().get_node("FloorGrid");
	ref_grid.visible = false;
	self.toggle_grid()
	
func toggle_grid():
	self.grids[0].visible = not grids[0].visible;
	self.grids[1].visible = not grids[1].visible;
	self.grids[2].visible = not grids[2].visible;
	


func _move_all_children(x: float, y: float):
	for c in self.get_children():
		c.transform.origin.x = x;
		c.transform.origin.y = y;

# Calculates the offset of all the layers in _render_data and
# updates _render_data[layer_number]['z_offset'] to be equally
# spaced based on _offset_in_z_layer
func _calculate_render_offsets():

	var layer_count := len(_render_data);
	var mid_layer:float = (float(layer_count) - 1.0)/2.0;
	for i in range(layer_count):
		var layer: Dictionary = _render_data[i];
		var diff:float = float(i) - mid_layer;
#		var norm_offset:int = floor(diff) if diff < 0 else ceil(diff);
		layer['z_offset'] = diff * _offset_in_z_per_layer;
#		print_debug("Setting layer %d offset to %f. Midlayer = %f" % [i, layer['z_offset'], mid_layer]);

func _move_layers_to_offsets():
	# moves all the childern to the
	# specified offset in _render_data
	for i in range(len(_render_data)):
		var layer: Dictionary = _render_data[i];
		# Invariants
		assert('z_offset' in layer);
		assert(typeof(layer['z_offset']) is int);

		layer['spatial'].transform.origin.z = layer['z_offset'];
		self.transform.origin

		if layer['below_via_spatial'] != null:
#			print_debug("Working on via %d" % i)
			layer['below_via_spatial'].transform.origin.z = float(_offset_in_z_per_layer)/2.0;
			var z_scale := _offset_in_z_per_layer;
			layer['below_via_spatial'].scale = Vector3(1,1,z_scale);

func _gltf_import(file_path: String) -> Spatial:
	var rv = self._importer.import_gltf(file_path);
	if rv == null:
		# null means that the file didn't load properly!
		printerr("ERROR! - expected to load a non-null layer got: ", file_path);
		print("Is there a proper .glb file at: ", file_path);
		assert(0); # check that you are loading from the right file location!
	return rv;
	
func scale_from(point: Vector3, new_scale: Vector3):
#	self.global_translate(-1 * point);
#	self.global_scale(new_scale / self.scale);
#	self.global_translate(point);
#	var sentinel_midpoint: Node = generate_midpoint_node();
	for child in self.get_children():
#		if not (child == sentinel_midpoint):
		child.translation -= self.to_local(point);
#	var scale_offset = 1;
	self.scale = new_scale;
#	_move_all_children(_global_offset.x, _global_offset.y);
	for child in self.get_children():
#		if not (child == sentinel_midpoint):
		child.translation+= self.to_local(point);
		
func set_rotation_around_point(point: Vector3, new_rotation: Vector3):
#	self.global_rotate()
#	for child in self.get_children():
#		child.translation -= self.to_local(point);
#	var sadd = new_rotation - self.rotation;
	self.rotation = new_rotation;
#	for child in self.get_children():
#		child.translation += self.to_local(point);
##	for child in self.get_children():
#		child.translation -= self.to_local(point);
#
#	self.scale = new_scale;
#	for child in self.get_children():
#		child.translation += self.to_local(point);
	


func add_net_to_path(path: String) -> String:
	return path.get_base_dir() + "/net/" + path.get_file();
# will be called when new data is availble from $Networking
# This function will load a layer with a specfic payload packet
# described here - https://docs.google.com/document/d/1ibc1CtqaPsl_z6HTpUybgDpXglcrLUYAqHou1LAIXf4/edit?usp=sharing
func load_layer_gltf(payload: Dictionary):
	
	for layer_num in payload:
		var layer_data: Dictionary = payload[layer_num];

		# Create _render_data dictionary object for this layer number
		if not layer_num in _render_data:
			_render_data[int(layer_num)] = {};
		else:
			print_debug("Overwriting layer #%d" % layer_num)

		print_debug("Adding layer " + layer_data['layerPath'])

		var gltf_layer := _gltf_import(layer_data['layerPath']);


		_render_data[int(layer_num)]['spatial'] = gltf_layer;
		
		
		# TODO UGLYYYY Fix to use like OS path
		if NET_HIGHLIGHT:
			_render_data[int(layer_num)]['spatial_highlight'] = _gltf_import(add_net_to_path(layer_data['layerPath']));
			_render_data[int(layer_num)]['spatial'].add_child(_render_data[int(layer_num)]['spatial_highlight']);

#		var _TODO_ = importer.import_gltf(layer_data["throughHoleMaskPath"]);
#		_render_data[layer_num]["throughHoleMaskPath"] = _TODO_;


		var via_path: String = layer_data['belowViasPath'];
		if via_path == '' or via_path == null:
			_render_data[int(layer_num)]['below_via_spatial'] = null
		else:
			var gltf_via := _gltf_import(via_path);
			_render_data[int(layer_num)]['below_via_spatial'] = gltf_via;
			# TODO UGLYYYY Fix to use like OS path
			if NET_HIGHLIGHT:
				_render_data[int(layer_num)]['below_via_spatial_highlight'] = _gltf_import(add_net_to_path(layer_data['belowViasPath']));
				_render_data[int(layer_num)]['below_via_spatial'].add_child(_render_data[int(layer_num)]['below_via_spatial_highlight']);
		_render_data[int(layer_num)]['display_name'] = layer_data['displayName'];

	self._calculate_render_offsets();

	self._update_children();
#	if self.get_child_count() > 0:
#		self._load_grid();
#func _load_grid():
#	var grid = load("res://Grid.tscn");
#	self.get_child(0).add_child(grid)
func _handle_keyboard_input(ev):
	var direction := -1 if  Input.is_key_pressed(KEY_SHIFT)else 1;

	if Input.is_key_pressed(KEY_E):
		self._is_expanding = self.movement_state.expanding;
	elif Input.is_key_pressed(KEY_D):
		self._is_expanding = self.movement_state.shrinking;
	elif Input.is_key_pressed(KEY_S):
		self._is_expanding = self.movement_state.stationary;

	# Reset the object
	if Input.is_key_pressed(KEY_R):
		self.rotation = Vector3(0, 0, 0)
		self.translate(-self.translation);

	# Rotate Object in space
	if Input.is_key_pressed(KEY_LEFT):
		self.rotate(Vector3.UP, ROTATION_SPEED * direction);
	elif Input.is_key_pressed(KEY_RIGHT):
		self.rotate(Vector3.DOWN, ROTATION_SPEED * direction);
	elif Input.is_key_pressed(KEY_UP):
		self.rotate(Vector3.RIGHT, ROTATION_SPEED * direction);
	elif Input.is_key_pressed(KEY_DOWN):
		self.rotate(Vector3.LEFT, ROTATION_SPEED * direction);
	elif Input.is_key_pressed(KEY_M):
		self.rotate(Vector3.FORWARD, ROTATION_SPEED * direction);
	elif Input.is_key_pressed(KEY_N):
		self.rotate(Vector3.BACK, ROTATION_SPEED * direction);

	#Toggle Auto seperation
	if Input.is_key_pressed(KEY_SPACE) and direction == -1:
		self._exploding_speed = -1;
	elif Input.is_key_pressed(KEY_SPACE):
		if self._exploding_speed != 0:
			self._exploding_speed = 0;
		elif self._exploding_speed == 0:
			self._exploding_speed = 1;

	var speed:float = KEYBOARD_MOVE_SPEED * float(direction);
	# Move object in space
	if Input.is_key_pressed(KEY_X):
		self.translate(Vector3(speed, 0, 0));
	elif Input.is_key_pressed(KEY_Y) or Input.is_key_pressed(KEY_C):
		self.translate(Vector3(0, speed, 0));
	if Input.is_key_pressed(KEY_Z):
		self.translate(Vector3(0, 0, speed));

	# Seperate childen manually
	if Input.is_key_pressed(KEY_T):
		self._set_z_offset(self._offset_in_z_per_layer + .01 * direction);


# sets the z offset to a new offset but ensures that it is atleast .1 large
func _set_z_offset(var new_offset):
	if new_offset >= MIN_SEPERATION:
		self._offset_in_z_per_layer = new_offset;
		self._calculate_render_offsets();

# Keyboard controls, will need seperate control scheme for when we have controllers.
# And those controls will be in each controller object
func _input(ev):
	self._handle_keyboard_input(ev);

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):

	self._move_layers_to_offsets();
	self._set_z_offset(self._offset_in_z_per_layer + EXPANSION_RATE * self._is_expanding);


