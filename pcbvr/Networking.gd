extends Node
"""
This node is responsable for networking with the websocket to KiCad server
"""

# Dictioary
signal load_board(board_layer_to_board_info);
# when we should reset board, clear the current rendering, rendering an empty scene. 
signal clear_board();
# when we should end the vr_client 
# message: String, in_error: Bool
signal end_vr(message, in_error);
# this is to set the position offset of the gltfs recieved
# x_pos_offset: float, y_pos_offset: float
signal gobal_position_offset(x_pos_offset, y_pos_offset);

# the base url 
const BASE_URL = "ws://127.0.0.1:"; # with the port after to connect

# what we send the server once connected sucessfully
var SEVER_SETUP_INFO = {
	"type": "setup",
	"payload": {
		"dumplocation": OS.get_user_data_dir()
	}
};

# starts the networking to provided port 
func start(the_port: String):
	var full_url = BASE_URL + the_port;
	print('Networking: Starting networking to ', full_url);
	_ws.connect_to_url(full_url);
	

# Appends the OS.get_user_data_dir to all the file names in the dictionary
# Strings in GDScript are non-nullable type and therefore if null is provided will
# return the empty string.
func _append_user_dir(path) -> String:
	if path == null: return "";
	var os_dir = OS.get_user_data_dir();
	if os_dir in path:
		return path;
	else:
		return os_dir + "/" + path;

var _ws = WebSocketClient.new();

# helper to send a debug message to server
func send_debug_message(message, extra = null):
	self._send(JSON.print({ "type": "debug", "payload": { "message": message, extra: extra } }))
	
func _ready():
	# this sets up the scallbacks to the websocket client
	_ws.connect("connection_established", self, "_connection_established")
	_ws.connect("connection_closed", self, "_connection_closed")
	_ws.connect("connection_error", self, "_connection_error")
	_ws.connect("data_received", self, "_data_recieved")
	# print_debug("Hello from networking node");

# once we connect
func _connection_established(protocol):
	print("Networking: Connection established with protocol: ", protocol)
	#_ws.get_peer(1).put_var("hello-from-godot")
	self._send(JSON.print(SEVER_SETUP_INFO));

# send message to server
func _send(msg: String):
	assert(_ws.get_connection_status() == _ws.CONNECTION_CONNECTED);
	_ws.get_peer(1).put_packet(msg.to_ascii());

# low level parsing of messages from websocket to a dictionary
func _data_recieved():
	if _ws.get_peer(1).is_connected_to_host():
		if _ws.get_peer(1).get_available_packet_count() > 0 :
			var data_str = _ws.get_peer(1).get_packet().get_string_from_utf8() 
			var result = JSON.parse(data_str);
			if (result.error == OK):
				self._handle_parsed_message(result.result);
			else:
				printerr("NETWORKING ERROR: expected data to be JSON but got ", data_str);

# handles a message from the KiCad server
func _handle_parsed_message(data: Dictionary): 
	print_debug('DEBUG: message recieved ', data);
	# we expect the following schema to handle a message from server
	assert(data.type);
	assert(data.payload);
	assert(typeof(data.payload) == TYPE_DICTIONARY);
	match data.type:
		"load-layer-gltf", "load-gltf-layer":
			print_debug("got `load-layer-gltf`");
			assert(typeof(data.payload) == TYPE_DICTIONARY);
			# this appends full-paths to everything
			for layerNumber in data.payload:
				var layer_info = data.payload[layerNumber];
				#Invariant checks
				assert(int(layerNumber) >= 0);
				assert("layerPath" in layer_info);
				assert("belowViasPath" in layer_info);
				assert("displayName" in layer_info);
				assert("throughHoleMaskPath" in layer_info);
				assert(layer_info['layerPath'] != null)
				assert(layer_info['layerPath'] != '')
				for field in layer_info:
					# maps all paths into full paths
					if "Path" in field:
						data.payload[layerNumber][field] = self._append_user_dir(data.payload[layerNumber][field]);
			emit_signal("clear_board");
			emit_signal("load_board", data.payload);
		"position":
			print_debug("got `position`");
			var x = data.payload.offset.x;
			var y = data.payload.offset.y;
			assert(typeof(x) == TYPE_REAL);
			assert(typeof(y) == TYPE_REAL);
			emit_signal("gobal_position_offset", x, y);
		"debug": 
			print("NETWORK DEBUG: ", data.payload.get("message", "<no-message>"));
			if data.payload.has("extra"): print("   extra", data.payload.get("extra"));
		"end":
			print_debug("got `end`");
			var in_error = data.payload.inError;
			var message = data.payload.message;
			assert(typeof(in_error) == TYPE_BOOL);
			assert(typeof(message) == TYPE_STRING);
			emit_signal("end_vr", message, in_error);
		_: printerr("ERROR! NETWORKING UNKNOWN TYPE: ", data);

# when the WS closes
func _connection_closed():
	print("Netorking: Connection closed")

# when there is an error with WS
func _connection_error():
	print("Networking: Connection error")
	print("Is the server connected?")
	assert(0);

# we want to continue to see if there is a message 
func _process(delta):
	if _ws.get_connection_status() == _ws.CONNECTION_CONNECTING || _ws.get_connection_status() == _ws.CONNECTION_CONNECTED:
		_ws.poll()
