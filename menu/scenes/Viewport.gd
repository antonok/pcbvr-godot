extends Viewport


# Declare member variables here. Examples:

# Get the mesh
onready var node_mesh = get_tree().get_root().get_node("MeshInstance");

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("pointer_pressed", self, "_on_pointer_pressed");

func _on_pointer_pressed(target, last_collided_at):
	print("Pointer pressed on ", target, " at ", last_collided_at);
	# TODO(mike) check if the target == node_mesh (or maybe the area?)
	
	# Get mesh size
	var mesh_size = node_mesh.mesh.size;
	
	# Find mouse position in Area
	var local_mouse_pos = node_mesh.global_transform.affine_inverse() * last_collided_at;
	
	# Scale to unit cube
	local_mouse_pos.x /= 2;
	local_mouse_pos.y /= 2;
	
	# Offset by half to base in top-left corner
	local_mouse_pos.x += 0.5;
	local_mouse_pos.y += 0.5;
	
	# Scale by pixel values
	local_mouse_pos.x *= size.x;
	local_mouse_pos.y *= size.y;

	# Create a mouse event to forward to the viewport
	var event = InputEventMouseButton.new();
	event.set_button_index(BUTTON_LEFT);
	event.set_doubleclick(false);
	event.set_pressed(true);
	
	event.set_global_position(local_mouse_pos);
	event.set_position(local_mouse_pos);
	
	input(event);
	
#func get_mouse_pos
