# pcbvr-godot

This is the frontend implementation for `nuboard`, a virtual-reality-assisted PCB design visualization tool. This frontend is intended to be launched from the `nuboard` fork of KiCad, which can be found [here](https://gitlab.com/antonok/kicad/-/tree/vr-master). Be sure to use the `vr-master` branch.

Note that a development branch of Godot was used to allow dynamic loading of GLTF resources at runtime. The upstream issue is tracked [here](https://github.com/godotengine/godot/issues/24768); we used rev `f00f49cf05c39db0d985e4327594bb10d5547ca4` of [GitHub user `fire`'s `scene_export_native_gltf` Godot branch](https://github.com/fire/godot/tree/scene_export_native_gltf).

SteamVR or another compatible OpenVR backend must already be running for this frontend to work correctly.

We've created a [quick demo video](https://www.youtube.com/watch?v=ZMEIp1qu0nU), as well as a [more technical view](https://www.youtube.com/watch?v=VWYOo4ThcNk) of `nuboard`'s full software architecture.
