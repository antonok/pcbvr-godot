extends Node
"""
Shares shared variables between the controllers.
"""


var offset_in_z: float;


var start_board_pos: Vector3;

var start_board_scale: Vector3;

# Cache distance between controllers
var start_separation: float;

# Cache midpoint between controllers
var last_midpoint: Vector3;
# Cache distance between controllers
var last_separation: float;
# Cache angle between controllers
var last_angle: float;


#strarting board roation 
var start_board_rotation: Vector3;

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
