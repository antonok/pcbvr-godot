# install godot

1. https://godotengine.org/download/
2. unzip and move the application to were you keep your applications
   (i.e. Applications on mac )

## running from the command line

### macOS

1. open terminal
2. `cd /Applications/Godot.app/Contents/MacOS/Godot`
3. `cp Godot godot`
4. `sudo chmod +x godot`
5. `mv godot /usr/local/bin/`

# godot script notes

- Scripts are attached to node
- Life cycle hooks on the nodes
  - Game start (enter tree)
  - Initialization (ready once children are ready and you are ready)
  - Create Game Loop
    - Get Input
    - Update Physics
    - Update World
    - Render
    - (loop)
  - Cleanup
  - Game End
- can create gdscript without nodes
- comments `#` no multi line comment
- whitespace dependent
- `pass` for empty line of code
- `print(<str>)`
- optional semi-colon `;`
- `var` - variables
- `const` - constants
- you can export variables/constants with `export`
- `export(<Type>) var <var name>` (typed exports)
-

# types

- example

```
var a = 1;  # godot uses duck typing it infers
same as
var a  = Int(1);
var b = 2.1 # float
var c = "three" #string
var d = Vector2(1,0) # back by c vector
var e = String(b) + c # string concat / casting
var arr = ["1", 1, "abc", 4] # arrays
[].append(<elm>)
for elm in arr: ...
```

## dictionary

```
var monster = {
  "happy" : false,
  "isAlive" : true,
  "score": 10
}
monster.happy # => false
```

## if

```
if(<expr>):
  pass
elif(<expr>):
  pass
else"
  pass
```

## match

```
var val = 6;
match val:
  1: pass
  6,7,8: print('hello') # you can keep test with `continue`
  _: print('this is the catch all')
```

# autoload

- runs in order to every time game is loaded
- `project` > `project settings` > `autoload`
- Access from any file `GameGlobals.<id>`

# running godot from command line

- https://docs.godotengine.org/en/3.1/getting_started/editor/command_line_tutorial.html#running-a-script
- `user@host:~/newgame$ godot -s sayhello.gd`

# https://www.youtube.com/watch?v=JuRhRhJ2914
