extends Spatial


# Declare member variables here. Examples:

# Get the mesh
var node_mesh: Node;
var node_viewport: Node;

# Called when the node enters the scene tree for the first time.
func _ready():
	node_mesh = $MeshInstance;
	node_viewport = $Viewport;
	assert(node_mesh != null);
	assert(node_viewport != null);
	pass;

func _on_pointer_pressed(target, last_collided_at):
	var debug_sphere = MeshInstance.new();
	debug_sphere.mesh = SphereMesh.new();
	self.add_child(debug_sphere);
	debug_sphere.translation = last_collided_at
	
	print("Pointer pressed on ", target, " at ", last_collided_at);
	# TODO(mike) check if the target == node_mesh (or maybe the area?)
	
	# Get mesh size
	var mesh_size = node_mesh.mesh.size;
	
	# Find mouse position in Area
	var local_mouse_pos = node_mesh.to_local(last_collided_at);
	
	print("Local mouse= "+ str(local_mouse_pos));
	
	# Scale to unit cube
	local_mouse_pos.x /= mesh_size.x;
	local_mouse_pos.y /= mesh_size.y;
	
	# Offset by half to base in top-left corner
	local_mouse_pos.x += 0.5;
	local_mouse_pos.y += 0.5;
	
	# Scale by pixel values
	local_mouse_pos.x *= node_viewport.size.x;
	local_mouse_pos.y *= node_viewport.size.y;

	# Create a mouse event to forward to the viewport
	var event = InputEventMouseButton.new();
	event.set_button_index(BUTTON_LEFT);
	event.set_doubleclick(false);
	event.set_pressed(true);
	
	var mouse_pos_2d = Vector2(local_mouse_pos.x, local_mouse_pos.y);
	event.set_global_position(mouse_pos_2d);
	event.set_position(mouse_pos_2d);
	
	node_viewport.input(event);
	print("Mouse pos: ", mouse_pos_2d);
	
#func get_mouse_pos
