#!/usr/bin/env node
const yargs = require("yargs");
const WebSocket = require("ws");
const readline = require("readline");

const getReadline = () =>
  readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

let rl = null;

// this filename should be saved at the user directory
const LAYER_0_EXAMPLE = "F.Cu.glb";
const LAYER_1_EXAMPLE = "B.Cu.glb";
const VIAS_BETWEEN_LAYER_0_TO_1_EXAMPLE = "vias.glb";

// Example of a two layer board unused...
// const ex1 =  {
//     0: {
//       layerPath: LAYER_0_EXAMPLE,
//       throughHoleMaskPath: null,
//       belowViasPath: VIAS_BETWEEN_LAYER_0_TO_1_EXAMPLE,
//       displayName: "Example Base Layer of a Board"
//     },
//     1: {
//       layerPath: LAYER_1_EXAMPLE,
//       throughHoleMaskPath: null,
//       belowViasPath: null,
//       displayName: "Example Top Layer of a Board"
//     }
//   };


const A64_OLINUXINO_EXAMPLE =  {
    0: {
      layerPath: 'F.Cu.glb',
      throughHoleMaskPath: null,
      belowViasPath: 'vias-F.Cu.glb',
      displayName: "Example Base Layer of a Board"
    },
    1: {
      layerPath: 'B.Cu.glb',
      throughHoleMaskPath: null,
      belowViasPath: null,
      displayName: "Example Top Layer of a Board"
    }
  };




// Object<Number, Object<String, String | null>> -> String: returns the request string with the object being the mapping
// of layer to file location
const makeGltfLoadRequest = (
  boardLayerNumberToLayerInfo = A64_OLINUXINO_EXAMPLE
) =>
  JSON.stringify({
    type: "load-layer-gltf",
    // Make sure you actually have this in the right godot user directory
    payload: boardLayerNumberToLayerInfo
  });

// String, Object -> String: returns the debug request with the message being
// the message and an object being any extra debug info
const makeDebugMessageRequest = (message = "", extra = {}) =>
  JSON.stringify({
    type: "debug",
    payload: { message, extra }
  });

// (String -> String | false) -> String[] Helper function to read to empty string
// using the function to validate user input. By default there is no validation.
// If there is an issue the parameter should return the issue as human text.
// WARNING: rl will be closed after this is called.
const readToEmptyLine = async (isLineInvalid = _line => false) => {
  const lines = [];
  rl.prompt();
  for await (const line of rl) {
    if (line == "") {
      console.log("done");
      break;
    }
    rl.prompt();
    const invalidReason = isLineInvalid(line);
    if (invalidReason) {
      console.log("Line invalid ", invalidReason);
    } else {
      lines.push(line);
    }
  }
  return lines;
};

// String -> Promise<String> | null: handles an input from the user from menu, if
// user didn't select valid menu option returns null. Otherwuse returns
// the request base on the user selection.
const handleInput = async answer => {
  console.debug("Handling user input ", answer);
  switch (answer) {
    case "1":
      console.log("Defualt board being sent");
      return makeGltfLoadRequest();
      break;
    case "2":
      {
        const numberOfLayer = await askQuestion(
          "How many layer board do you want to send?"
        );
        const result = {};
        for (i = 0; i < numberOfLayer; i++) {
          const layerPath = await askQuestion(`Path of layer ${i}?`);
          const throughHoleMaskPath =
            (await askQuestion(
              `Through hole mask of layer ${i}, press enter if none?`
            )) || null;
          const belowViasPath =
            (await askQuestion(
              `Below vias of layer ${i}, press enter if none?`
            )) || null;
          const displayName =
            (await askQuestion(
              `Display name of layer ${i}, press enter if none?`
            )) || null;
          result[i] = {
            layerPath,
            throughHoleMaskPath,
            belowViasPath,
            displayName
          };
        }
        return makeGltfLoadRequest(result);
      }
      break;
    case "3":
      {
        console.log("Write JSON, press enter when done");
        const input = (await readToEmptyLine()).join("");
        try {
          JSON.parse(input);
          console.log("sending...");
          return input;
        } catch (e) {
          console.log("invalid JSON input", e);
        }
      }
      break;
    case "4":
      {
        console.log("Write debug message string to send");
        const input = (await readToEmptyLine()).join("");
        return makeDebugMessageRequest(input);
      }
      break;
    case "5":
      console.log("position offset");
      {
        const x = Number.parseFloat(await askQuestion("offset x?"));
        const y = Number.parseFloat(await askQuestion("offset y?"));
        if (Number.isNaN(x) || Number.isNaN(y)) {
          console.log("Input is invalid both x and y must be numbers!");
          return handleUserInput("5");
        }
        return JSON.stringify({
          type: "position",
          payload: {
            offset: {
              x,
              y
            }
          }
        });
      }
    case "6":
      console.log("end command");
      return JSON.stringify({
        type: "end",
        payload: {
          message: await askQuestion("What message would you like to send?"),
          inError: ["y", "yes", "t", "true"].includes(
            (await askQuestion("Are we ending in error?")).trim().toLowerCase()
          )
        }
      });
    default:
      console.log(`Invalid input ${answer}`);
      return null;
  }
};

// helper to make rl.question into an async function.
const askQuestion = question =>
  new Promise((res, rej) => {
    rl.question(question + "\n> ", answer => {
      res(answer);
    });
  });

// WebSocket -> Void: looks with the WebSocket asking user question.
const handleUserInput = async ws => {
  // https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/readyState
  if (ws.readyState != 1) {
    rl.close();
    console.log("WebSocket Not Connected");
    return;
  }

  rl = getReadline();
  const answer = await askQuestion(
    "What do you want to do?\n\
      1 - send dummy `load-layer-gltf`\n\
      2 - send custom `load-layer-gltf`\n\
      3 - handwrite json\n\
      4 - send debug message\n\
      5 - send offseting information\n\
      6 - send end\n"
  );
  const request = await handleInput(answer);
  if (request) await ws.send(request);
  rl.close();
  return handleUserInput(ws);
};

// MAIN
const main = (() => {
  const argv = yargs
    .option("port", {
      alias: "p",
      default: "8080",
      describe: "port to use when creating the WebSocket",
      type: "string"
    })
    .option("verbose", {
      alias: "v",
      type: "boolean",
      default: true,
      description: "Run with verbose logging"
    })
    .option("autoSend", {
      alias: "as",
      type: "boolean",
      default: true,
      description: "Flag to auto send on connection dummy board"
    })
    .demandOption(
      ["port"],
      "Please provide both run and path arguments to work with this tool"
    )
    .help()
    .alias("help", "h").argv;

  if (!argv.verbose) {
    console.debug = () => null;
  }
  const port = argv.port;
  console.debug("opening WebSocket on port", port);
  const wss = new WebSocket.Server({ port });
  console.log("Start Godot client");
  console.log("Awaiting Connection to Godot Client...");
  wss.on("connection", ws => {
    console.debug("Got a connections");
    ws.on("message", message => {
      if (rl) rl.close();
      try {
        JSON.parse(message);
      } catch (e) {
        console.error(
          "ERROR! message received was not json; KiCad expects json!!!",
          e
        );
      }
      console.log(`Received message from Godot => ${message}`);
      handleUserInput(ws);
    });
    // only send auto messages if enabled from command line.
    if (argv.autoSend) {
      console.debug("Sending `debug` message");
      ws.send(makeDebugMessageRequest("Hello from NodeJS Mock Server"));
      console.debug("Sending `load-layer-gltf`");
      ws.send(makeGltfLoadRequest());
    }
  });
})();
